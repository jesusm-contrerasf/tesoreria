</div>


</body>

<section>
         <!-- Footer -->
<footer class="page-footer font-small blue-grey lighten-5 " style="background-color: #FFFFFF;">

<div style="background-color: #17A2B8;">
  <div class="container">

    <!-- Grid row-->
    <div class="row py-4 d-flex align-items-center">

      <!-- Grid column -->
      <div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">
        <h6 class="mb-0">Mantente en contacto con nosotros en  nuestras redes sociales!</h6>
      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-6 col-lg-7 text-center text-md-right">

        <!-- Facebook -->
        <a class="fb-ic">
          <i class="fab fa-facebook-f white-text mr-4"> </i>
        </a>
        <!-- Twitter -->
        <a class="tw-ic">
          <i class="fab fa-twitter white-text mr-4"> </i>
        </a>
        <!-- Google +-->
        <a class="gplus-ic">
          <i class="fab fa-google-plus-g white-text mr-4"> </i>
        </a>
        <!--Linkedin -->
        <a class="li-ic">
          <i class="fab fa-linkedin-in white-text mr-4"> </i>
        </a>
        <!--Instagram-->
        <a class="ins-ic">
          <i class="fab fa-instagram white-text"> </i>
        </a>

      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row-->

  </div>
</div>

<!-- Footer Links -->
<div class="container text-center text-md-left mt-5" style="background-color: #FFFFFF;">
    

  <!-- Grid row -->
  <div class="row mt-3 dark-grey-text">

    <!-- Grid column -->
    <div class="col-md-4 col-lg-4 col-xl-3 mb-4">

      <!-- Content -->
      <h6 class="text-uppercase font-weight-bold">Acerca de nosotros</h6>
      <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
      <p>Pets life es una plataforma pensada y diseñada para usuarios que deseen compartir la vida de sus mascotas
        con lo demás usuarios, además de sus fotos y actividades diarias.
      </p>

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
 
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-md-4 col-lg-2 col-xl-2 mx-auto mb-4">
        

      <!-- Links -->
      <h6 class="text-uppercase font-weight-bold">Nuestros usuarios</h6>
      <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
      <p>
        <a class="dark-grey-text" href="#!">Tu Cuenta</a>
      </p>
      <p>
        <a class="dark-grey-text" href="#!">Registrate</a>
      </p>
      <p>
        <a class="dark-grey-text" href="#!">Información</a>
      </p>
      <p>
        <a class="dark-grey-text" href="#!">Ayuda</a>
      </p>

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">

      <!-- Links -->
      <h6 class="text-uppercase font-weight-bold">Contacto</h6>
      <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
      <p>
        <i class="fas fa-home mr-3"></i> AV 15 N_11-120 BR EL CONTENTO  </p>
      <p>
        <i class="fas fa-envelope mr-3"></i> petslife@hotmail.com</p>
      <p>
        <i class="fas fa-phone mr-3"></i> + 5575674</p>
      <p>
        

    </div>
    <!-- Grid column -->

  </div>
  <!-- Grid row -->

</div>
<!-- Footer Links -->

<!-- Copyright -->
<div class="footer-copyright text-center text-black-50 py-3">© 2020 Copyright:
  <a class="dark-grey-text" href="#"> Armandotroqueles2020.com</a>
</div>
<!-- Copyright -->

</footer>
<!-- Footer -->
    </section>

</html>